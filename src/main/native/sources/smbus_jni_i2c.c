#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>  // for open
#include <unistd.h> // for close

#include <asm/ioctl.h>

#include <linux/types.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include <sys/ioctl.h>
#include <i2c/smbus.h>

#include "eu_galjente_io_i2c_SmbusI2C.h"

#define IO_EXCEPTION_CLASS_NAME "java/io/IOException"

void debugMessage(JNIEnv *env, jclass classI2C, const char *message, ...) {
    jfieldID enabledId = (*env)->GetStaticFieldID(env, classI2C, "nativeDebug", "Z");
    jboolean enabled = (*env)->GetStaticBooleanField(env, classI2C, enabledId);
    if (enabled) {
        va_list arg;

        va_start(arg, message);
            vprintf(message, arg);
        va_end(arg);
        fflush(stdout);
    }
}

void throwNewException(JNIEnv *env, const char *className, const char *message, ...) {
    va_list arg;
    char messageBuffer[255];

    va_start(arg, message);
        vsnprintf(messageBuffer, sizeof(messageBuffer), message, arg);
    va_end(arg);

    jclass exceptionClass = (*env)->FindClass(env, className);
    (*env)->ThrowNew(env, exceptionClass, messageBuffer);
}

jint getFileDescriptor(JNIEnv *env, jobject thisObj, jclass classI2C) {
    jfieldID fdFieldId = (*env)->GetFieldID(env, classI2C, "fileDescriptor", "I");
    return (*env)->GetIntField(env, thisObj, fdFieldId);
}

bool checkFileDescriptorIsOpen(JNIEnv *env, jclass classI2C, int fileDescriptor) {
    if (fileDescriptor <= 0) {
        debugMessage(env, classI2C, "I2C device is closed\r\n");
        throwNewException(env, IO_EXCEPTION_CLASS_NAME, "I2C device is closed");
    }
}

void checkI2CResults(JNIEnv *env, jclass classI2C, __s32 result) {
    if (result < 0) {
        debugMessage(env, classI2C, "I2C operation failed: %s\r\n", strerror(errno));
        throwNewException(env, IO_EXCEPTION_CLASS_NAME, "I2C operation failed: %s", strerror(errno));
    }
}

JNIEXPORT void JNICALL Java_eu_galjente_io_i2c_SmbusI2C_open(JNIEnv *env, jobject thisObj, jstring i2cDevice, jbyte address) {
    jclass classI2C = (*env)->GetObjectClass(env, thisObj);
    debugMessage(env, classI2C, "Call \"Java_eu_galjente_io_i2c_SmbusI2C_open\"\r\n");
    
    jfieldID fdFieldId = (*env)->GetFieldID(env, classI2C, "fileDescriptor", "I");
    const char *i2cDeviceChar = (*env)->GetStringUTFChars(env, i2cDevice, NULL);

    debugMessage(env, classI2C, "Opening I2C \"%s\"\r\n", i2cDeviceChar);
    int fd = open(i2cDeviceChar, O_RDWR);

    (*env)->SetIntField(env, thisObj, fdFieldId, (jint)fd);
    (*env)->ReleaseStringUTFChars(env, i2cDevice, i2cDeviceChar);

    if (fd < 0) {
        debugMessage(env, classI2C, "Unable to open I2C device: %s\r\n", strerror(errno));
        throwNewException(env, IO_EXCEPTION_CLASS_NAME, "Unable to open I2C device: %s", strerror(errno));
    }

    debugMessage(env, classI2C, "I2C opened, selecting device \"%d\"\r\n", address);
    if (ioctl(fd, I2C_SLAVE, address) < 0) {
        Java_eu_galjente_io_i2c_SmbusI2C_close(env, thisObj);
        debugMessage(env, thisObj, "Unable to select I2C device: %s\r\n", strerror(errno));
        throwNewException(env, IO_EXCEPTION_CLASS_NAME, "Unable to select I2C device: %s", strerror(errno));
    }

    debugMessage(env, classI2C, "Device \"%d\" selected\r\n", address);
}

JNIEXPORT void JNICALL Java_eu_galjente_io_i2c_SmbusI2C_close(JNIEnv *env, jobject thisObj) {
    jclass classI2C = (*env)->GetObjectClass(env, thisObj);
    debugMessage(env, classI2C, "Call \"Java_eu_galjente_io_i2c_SmbusI2C_close\"\r\n");
    jfieldID fdFieldId = (*env)->GetFieldID(env, classI2C, "fileDescriptor", "I");
    jint fd = (*env)->GetIntField(env, thisObj, fdFieldId);

    if (fd <= 0) {
        debugMessage(env, classI2C, "I2C device is not opened\r\n");
        return;
    }

    if (close(fd) < 0) {
        debugMessage(env, classI2C, "Unable to close I2C device: %s\r\n", strerror(errno));
        throwNewException(env, IO_EXCEPTION_CLASS_NAME, "Unable to close I2C device: %s", strerror(errno));
    }

    (*env)->SetIntField(env, thisObj, fdFieldId, 0);
    debugMessage(env, classI2C, "I2C device closed\r\n");
}

JNIEXPORT jbyte JNICALL Java_eu_galjente_io_i2c_SmbusI2C_read(JNIEnv *env, jobject thisObj) {
    jclass classI2C = (*env)->GetObjectClass(env, thisObj);
    debugMessage(env, classI2C, "Call \"Java_eu_galjente_io_i2c_SmbusI2C_read\"\r\n");
    jint fd = getFileDescriptor(env, thisObj, classI2C);
    checkFileDescriptorIsOpen(env, classI2C, fd);

    debugMessage(env, classI2C, "Reading value from I2C device\r\n");
    __s32 result = i2c_smbus_read_byte(fd);
    checkI2CResults(env, classI2C, result);

    debugMessage(env, classI2C, "Value is %i\r\n", result);
    return result;
}

JNIEXPORT jbyte JNICALL Java_eu_galjente_io_i2c_SmbusI2C_readRegister(JNIEnv *env, jobject thisObj, jbyte i2cRegister) {
    jclass classI2C = (*env)->GetObjectClass(env, thisObj);
    debugMessage(env, classI2C, "Call \"Java_eu_galjente_io_i2c_SmbusI2C_readRegister\"\r\n");
    jint fd = getFileDescriptor(env, thisObj, classI2C);
    checkFileDescriptorIsOpen(env, classI2C, fd);

    debugMessage(env, classI2C, "Reading register value from I2C device\r\n");
    __s32 result = i2c_smbus_read_byte_data(fd, i2cRegister);
    checkI2CResults(env, classI2C, result);

    debugMessage(env, classI2C, "Register value is %i\r\n", result);
    return result;
}

JNIEXPORT jshort JNICALL Java_eu_galjente_io_i2c_SmbusI2C_readWordRegister(JNIEnv *env, jobject thisObj, jbyte i2cRegister) {
    jclass classI2C = (*env)->GetObjectClass(env, thisObj);
    debugMessage(env, classI2C, "Call \"Java_eu_galjente_io_i2c_SmbusI2C_readWordRegister\"\r\n");
    jint fd = getFileDescriptor(env, thisObj, classI2C);
    checkFileDescriptorIsOpen(env, classI2C, fd);

    debugMessage(env, classI2C, "Reading word register value from I2C device\r\n");
    __s32 result = i2c_smbus_read_word_data(fd, i2cRegister);
    checkI2CResults(env, classI2C, result);

    debugMessage(env, classI2C, "Word register value is %i\r\n", result);
    return result;
}

JNIEXPORT void JNICALL Java_eu_galjente_io_i2c_SmbusI2C_write(JNIEnv *env, jobject thisObj, jbyte value) {
    jclass classI2C = (*env)->GetObjectClass(env, thisObj);
    debugMessage(env, classI2C, "Call \"Java_eu_galjente_io_i2c_SmbusI2C_write\"\r\n");
    jint fd = getFileDescriptor(env, thisObj, classI2C);
    checkFileDescriptorIsOpen(env, classI2C, fd);

    debugMessage(env, classI2C, "Writing value to I2C device\r\n");
    __s32 result = i2c_smbus_write_byte(fd, value);
    checkI2CResults(env, classI2C, result);
    
    debugMessage(env, classI2C, "Value was written to I2C device\r\n");
}

JNIEXPORT void JNICALL Java_eu_galjente_io_i2c_SmbusI2C_writeRegister__BB(JNIEnv *env, jobject thisObj, jbyte i2cRegister, jbyte value) {
    jclass classI2C = (*env)->GetObjectClass(env, thisObj);
    debugMessage(env, classI2C, "Call \"Java_eu_galjente_io_i2c_SmbusI2C_writeRegister__BB\"\r\n");
    jint fd = getFileDescriptor(env, thisObj, classI2C);
    checkFileDescriptorIsOpen(env, classI2C, fd);

    debugMessage(env, classI2C, "Writing register value to I2C device\r\n");
    __s32 result = i2c_smbus_write_byte_data(fd, i2cRegister, value);
    checkI2CResults(env, classI2C, result);
    
    debugMessage(env, classI2C, "Register value was written to I2C device\r\n");
}

JNIEXPORT void JNICALL Java_eu_galjente_io_i2c_SmbusI2C_writeRegister__BS(JNIEnv *env, jobject thisObj, jbyte i2cRegister, jshort value) {
    jclass classI2C = (*env)->GetObjectClass(env, thisObj);
    debugMessage(env, classI2C, "Call \"Java_eu_galjente_io_i2c_SmbusI2C_writeRegister__BS\"\r\n");
    jint fd = getFileDescriptor(env, thisObj, classI2C);
    checkFileDescriptorIsOpen(env, classI2C, fd);

    debugMessage(env, classI2C, "Writing word register value to I2C device\r\n");
    __s32 result = i2c_smbus_write_word_data(fd, i2cRegister, value);
    checkI2CResults(env, classI2C, result);
    
    debugMessage(env, classI2C, "Word register value was written to I2C device\r\n");
}
