#!/bin/sh

CURRENT_DIR=$(pwd)
SOURCE_DIR=$( dirname -- "$( readlink -f -- "$0"; )"; )
BUILD_DIR="${SOURCE_DIR}/build"
HEADER_DEST_DIR="${SOURCE_DIR}/include"
JAVA_SOURCE_FILE_DIR=$( readlink -f -- "../java"; )
JAVA_SOURCE_FILE_PATH="$JAVA_SOURCE_FILE_DIR/eu/galjente/io/i2c/SmbusI2C.java"

SOURCE_SO_FILE_NAME=libsmbus_jni.so
DEST_SO_FILE_NAME=smbus_jni_i2c.so
DEST_SHA_FILE_NAME=smbus_jni_i2c.sha256
DEST_SO_FILE_PATH="../../resources/${DEST_SO_FILE_NAME}"
DEST_SHA_FILE_PATH="../../resources/${DEST_SHA_FILE_NAME}"

help() {
  echo "Smbus I2C builder help"
  echo "--help - Show this help"
  echo "--build-header classpath - Generate C headers from Java class"
  echo "--build - Compile, link calculate hash and move to resources folder"
  echo "--clear-build - Remove build folder"
}

build_header() {
  echo "Building Smbus I2C header from Java class"
  "$JAVA_HOME/bin/javac" -classpath "$1" -d "$BUILD_DIR" -h "$HEADER_DEST_DIR" "$JAVA_SOURCE_FILE_PATH" || echo "Smbus I2C header generation failed" && exit
  echo "Smbus I2C header generated"
}

build() {
  echo "Building Smbus I2C native library..."
  if [ -d "${BUILD_DIR}" ]; then
    echo "Switching to ${BUILD_DIR}"
    cd "${BUILD_DIR}" || echo "Failed switch folder to ${SOURCE_DIR}/build" && return
  else
    echo "Creating dir ${BUILD_DIR}"
    mkdir "${BUILD_DIR}"

    echo "Switching to ${BUILD_DIR}"
    cd "${BUILD_DIR}" || echo "Failed switch folder to ${BUILD_DIR}" && return

    echo "Running cmake"
    cmake ../
  fi

  echo "Running make"
  make

  echo "Checking result file '${DEST_SO_FILE_NAME}'"
  if [ -f "libsmbus_jni.so" ]; then
    cp "${SOURCE_SO_FILE_NAME}" "$DEST_SO_FILE_PATH"
    sha256sum $DEST_SO_FILE_PATH | grep -o '^\w*\b' | tr -d "\n\r" > $DEST_SHA_FILE_PATH
    echo "File '${DEST_SO_FILE_NAME}' copied to ${DEST_SO_FILE_PATH}"
  else
    echo "File '${DEST_SO_FILE_NAME}' missing"
  fi

  echo "Returning back to ${CURRENT_DIR}"
  cd "${CURRENT_DIR}" || echo "Failed return to original folder ${CURRENT_DIR}"
  echo "Smbus I2C native library build"
}

clear_build() {
  echo "Clear build folder"
  rm -R "${BUILD_DIR}"
  echo "Build folder cleared"
}

echo "Checking JAVA_HOME=$JAVA_HOME"
if [ -d "$JAVA_HOME" ]; then
  if [ "$1" != "" ]; then
    while [ "$1" != "" ]; do
      case $1 in
        --help ) help;;
        --build-header ) build_header "$2";;
        --build ) build;;
        --clear-build ) clear_build;;
      esac
      shift
    done
  else
    help
  fi
else
  echo "JAVA_HOME is missing"
fi
