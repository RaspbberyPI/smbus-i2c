package eu.galjente.io.i2c;

/**
 * Signals that I2C device is not found in system.
 */
public class I2CDeviceNotFoundException extends I2CException {

    /**
     * Constructs an {@code I2CDeviceNotFoundException} with {@code null} as its error detail message.
     */
    public I2CDeviceNotFoundException() {
        super();
    }

    /**
     * Constructs an {@code I2CDeviceNotFoundException} with the specified detail message.
     *
     * @param message The detail message (which is saved for later retrieval by the {@link #getMessage()} method)
     */
    public I2CDeviceNotFoundException(String message) {
        super(message);
    }

}
