package eu.galjente.io.i2c;

import java.io.IOException;

/**
 * This is I2C API implementation using smbus.
 */
public class SmbusI2C implements I2C {

    static boolean nativeDebug = false; // used in JNI library

    private final String i2cDevice;
    private final byte address;
    private int fileDescriptor = 0; // used in JNI library

    /**
     * Create I2C instance and establish connection with I2C device by address and I2C system file.
     * @param i2cDevice path to system I2C file
     * @param address device address
     * @throws IOException throw when I2C file couldn't be open or failed obtain device by address.
     */
    SmbusI2C(String i2cDevice, byte address) throws IOException {
        this.i2cDevice = i2cDevice;
        this.address = address;
        open(i2cDevice, address);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public native synchronized byte read() throws IOException;

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized native byte readRegister(byte register) throws IOException;

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized native short readWordRegister(byte register) throws IOException;

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized native void write(byte value) throws IOException;

    /**
     * {@inheritDoc}
     */
    @Override
    public native synchronized void writeRegister(byte register, byte value) throws IOException;

    /**
     * {@inheritDoc}
     */
    @Override
    public native synchronized void writeRegister(byte register, short value) throws IOException;

    /**
     * {@inheritDoc}
     */
    @Override
    public native synchronized void close() throws Exception;

    /**
     * Establish connection with I2C device by address and I2C system file.
     *
     * @param i2cDevice path to system I2C file.
     * @param address device address.
     * @throws IOException throw when I2C file couldn't be open or failed obtain device by address.
     */
    private native synchronized void open(String i2cDevice, byte address) throws IOException;

    /**
     * I2C system device path.
     *
     * @return path to system I2C file.
     */
    public String getI2cDevice() {
        return i2cDevice;
    }

    /**
     * I2C device address.
     *
     * @return byte address.
     */
    public byte getAddress() {
        return address;
    }

    /**
     * Opened file descriptor identification.
     *
     * @return identification.
     */
    public int getFileDescriptor() {
        return fileDescriptor;
    }
}
