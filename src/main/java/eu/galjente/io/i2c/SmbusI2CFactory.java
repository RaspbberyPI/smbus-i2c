package eu.galjente.io.i2c;

import eu.galjente.jni.NativeLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public class SmbusI2CFactory implements I2CFactory {

    static {
        NativeLoader.create("smbus_jni_i2c").load();
    }

    private static final String I2C_DEVICES_PATH = "/dev";
    private static final String I2C_DEVICE_PREFIX = I2C_DEVICES_PATH + "/i2c";

    private static final I2CFactory INSTANCE = new SmbusI2CFactory();

    /**
     * Obtain {@code SmbusI2CFactory} from memory and return interface {@code I2CFactory}.
     *
     * @return {@code I2CFactory} instance.
     */
    public static I2CFactory getInstance() {
        return INSTANCE;
    }

    /**
     * Singleton class can have just one instance.
     */
    private SmbusI2CFactory() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public I2C create(byte address) throws IOException {
        final List<String> i2cDevices = getI2CDevices();
        if (i2cDevices.isEmpty()) {
            throw new I2CDeviceNotFoundException("I2C devices not found");
        }
        return new SmbusI2C(i2cDevices.get(0), address);
    }

    /**
     * Create I2C device client based on I2C system file and I2C device address.
     *
     * @param i2cDevice path to I2C system file.
     * @param address I2C device address
     * @return {@code I2C} with established connection to I2C device.
     * @throws IOException throws whn failed open I2C system file or establish connection to I2C device.
     */
    public I2C create(String i2cDevice, byte address) throws IOException {
        if (!isDeviceExist(i2cDevice)) {
            throw new I2CDeviceNotFoundException("I2C device \"" + i2cDevice + "\" not found");
        }
        return new SmbusI2C(i2cDevice, address);
    }

    /**
     * Get list of available I2C system files.
     *
     * @return list of I2C system files path.
     * @throws IOException when failed obtain fail list from "\dev" directory.
     */
    public List<String> getI2CDevices() throws IOException {
        try (Stream<Path> devices = Files.list(Path.of(I2C_DEVICES_PATH))) {
            return devices
                    .map(Path::toString)
                    .filter(value -> value.startsWith(I2C_DEVICE_PREFIX))
                    .collect(Collectors.toList());
        }
    }

    /**
     * Check is I2C system file path point to existed fail.
     *
     * @param i2cDevice path to system I2C file.
     * @return true if file exist and false when file is missing.
     * @throws I2CException when file is missing or failed check file existence.
     */
    public boolean isDeviceExist(String i2cDevice) throws I2CException {
        if (!i2cDevice.startsWith(I2C_DEVICE_PREFIX)) {
            throw new I2CException("Unknown i2c device: \"" + i2cDevice + "\"");
        }
        return Files.exists(Path.of(i2cDevice));
    }

    /**
     * Enable or disable logging in native fail.
     *
     * @param enable true to enable log and false to disable log.
     */
    public static void setNativeDebug(boolean enable) {
        SmbusI2C.nativeDebug = enable;
    }

}
