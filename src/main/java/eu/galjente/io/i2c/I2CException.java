package eu.galjente.io.i2c;

import java.io.IOException;

/**
 * Signals that some sort of exception occurred in I2C.
 * This class is the general class of exceptions produced by failed or interrupted I2C operations.
 */
public class I2CException extends IOException {

    /**
     * Constructs an {@code I2CException} with {@code null} as its error detail message.
     */
    public I2CException() {
        super();
    }

    /**
     * Constructs an {@code I2CException} with the specified detail message.
     *
     * @param message The detail message (which is saved for later retrieval by the {@link #getMessage()} method)
     */
    public I2CException(String message) {
        super(message);
    }

}
