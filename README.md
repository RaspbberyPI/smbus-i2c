# Smbus I2C
This is implementation of [I2C Api](https://gitlab.com/RaspbberyPI/i2c-api). 
Implementation based on Linux Smbus.
**Native library was compiled on Linux(debian) and tested on Raspberry Pi 4**

## Technologies
* Java 11
* Maven 3
* JNI
* C/C++

## Built With
* [Maven](https://maven.apache.org/) - Dependency Management
* CMake/make - C/C++ Compiler

## Compile application
```bash
    cd src/main/native
    ./build.sh --build
    ./mvnw compile
```

### Install to local maven
```bash
  ./mvnw install
```

## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/RaspbberyPI/smbus-i2c/-/tags).

## License ![GitLab](https://img.shields.io/gitlab/license/RaspbberyPI/smbus-i2c)
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Authors
* **Arturs Cvetkovs** - *Initial work* - [Galjente](https://gitlab.com/Galjente)
